<!DOCTYPE html>
<html>
<head>
	<hr color=black>
	<title>Software Requirements and Specifications</title>
	<style type="text/css">
	@media print {
    #printbtn {
        display :  none;
   			 }
		}
	</style>
	<h1><center>Software Requirements Specification</center></h1>
	<br><h4 align="center">For</h4>
	<h2><center>Medical Report System</center></h2>
	<br><br>
		<h4 align="center">Version 1.0</h4>
		<br><br>
		<h3 align="center"><font size="4">Prepared by -</font></h3>
		<p align="center"> <font size="4">
			<center>
               <table border="0" width="800" height="150">
               	
               	<tr>   <th>Name</th>    <th>ID</th>  <th>Email</th> </tr>
                
                <tr>   <td>Mohammad Tauseeful Haque</td>    <td>1711845042</td>  <td> tauseeful30@gmail.com</td> </tr>
                <tr>   <td>Marfau Ahned</td>    <td>1721729042</td>  <td>meahmed0307@gmail.com</td> </tr>
                <tr>   <td>Afia Fariha</td>    <td>1712393042</td>  <td>afia.fariha@northsouth.edu</td> </tr>
                <tr>   <td>Sadia Alam</td>    <td>1620263042</td>  <td>sadiaelma0167@gmail.com</td> </tr>
               </table></center></p>
		<br><br>
		<h3 align="center"><font size="4">Instructor: Dr. Nabeel Mohammed</font></h3>
		
		<h3 align="center"><font size="4">Course: CSE327</font></h3>
		
        <h3 align="center"><font size="4">Section: 2</font></h3>
		
        <h3 align="center"><font size="4">Date: 4/02/2020</font></h3>
		
		</p> 
		<br><br>
</head>
<body>
	<p style="page-break-after:always"></p>
	<center>
	<h2>Table of Contents</h2>
	<p style="line-height:1.10">

		<span><b>CONTENTS</b>&emsp;
		......................................................................................................................................I</span><br>
		<p><span><b>REVISIONS</b>&emsp;
		......................................................................................................................................II</span><br>
		</p>
		<p><span><b>1.&emsp;INTRODUCTION</b>&emsp;
		....................................................................................................................01</span><br>
		<span><b>&emsp;1.1&emsp;DOCUMENT PURPOSE</b>&emsp;&nbsp;&nbsp;..................................................................................................01</span><br>
		<span><b>&emsp;1.2&emsp;PRODUCT SCOPE</b>&emsp;
		............................................................................................................01</span><br>
		<span><b>&emsp;1.3&emsp;INTENDED AUDIENCE AND DOCUMENT OVERVIEW</b>&emsp;
		.........................................01</span><br>
		<span><b>&emsp;1.4&emsp;DEFINITIONS, ACRONYMS AND ABBREVIATIONS</b>&emsp;................................................01</span><br>
		<span><b>&emsp;1.5&emsp;DOCUMENT CONVENTIONS</b>&emsp;
		........................................................................................01</span><br>
		<span><b>&emsp;1.6&emsp;REFERENCES AND ACKNOWLEDGMENTS</b>&emsp;
		.............................................................01</span><br></p>


		<p><span><b>2.&emsp;OVERALL DESCRIPTION</b>&emsp;
		.....................................................................................................02</span><br>
		<span><b>&emsp;2.1&emsp;PRODUCT OVERVIEW</b>&emsp;
		....................................................................................................02</span><br>
		<span><b>&emsp;2.2&emsp;PRODUCT FUNCTIONALITY</b>&emsp;
		.........................................................................................02</span><br>
		<span><b>&emsp;2.3&emsp;DESIGN AND IMPLEMENTATION CONSTRAINTS</b>&emsp;
		..................................................02</span><br>
		<span><b>&emsp;2.4&emsp;ASSUMPTIONS AND DEPENDENCIES</b>&emsp;
		.........................................................................02</span><br>
		</p>
		<p><span><b>3.&emsp;SPECIFIC REQUIREMENTs</b>&emsp;
		...................................................................................................03</span><br>
		<span><b>&emsp;3.1&emsp;EXTERNAL INTERFACE REQUIREMENTS</b>&emsp;
		................................................................03</span><br>
		<span><b>&emsp;3.2&emsp;FUNCTIONAL REQUIREMENTS</b>&emsp;.....................................................................................03</span><br>
		<span><b>&emsp;3.3&emsp;USE CASE MODEL</b>&emsp;..............................................................................................................04</span><br></p>
		
		<p><span><b>4.&emsp;OTHER NON-FUNCTIONAL REQUIREMENTS</b>&emsp;
		.................................................................05</span><br>
		<span><b>&emsp;4.1&emsp;PERFORMANCE REQUIREMENTS</b>&emsp;.................................................................................05</span><br>
		<span><b>&emsp;4.2&emsp;SAFETY AND SECURITY REQUIREMENTS</b>&emsp;..................................................................05</span><br>
		<span><b>&emsp;4.3&emsp;SOFTWARE QUALITY ATTRIBUTES</b>&emsp;..............................................................................05</span><br></p>

		<p><span><b>5.&emsp;OTHER REQUIREMENTS</b>&emsp;
		.......................................................................................................06</span><br></p>
		
		<p><span><b> APPENDIX A – DATA DICTIONARY </b>&emsp;
		............................................................................................07</span><br></p>
		<p><span><b> APPENDIX B - GROUP LOG    </b>&emsp;
		..........................................................................................................08</span><br>
		</p></center>
	</p>
	</p>
	<p style="page-break-after:always"></p>	
    <h2 align="left">1.	Introduction</h2>
	<h4 align="left">&emsp;1.1&emsp;Document Purpose</h4>
	<p>
		<blockquote>
			The purpose of this project is to make a "Medical Report" which will help the users to identify and see the details of his or her medical report in a visual way. This software will also help the user to store any medical data and use the data in future for taking any medicine according to the previous prescriptions.

		</blockquote>
	</p>

	<h4 align="left">&emsp;1.2&emsp;Product Scope</h4>
	<p>
		<blockquote>
			This system is designed to provide users with the necessary information needed while he or she may not going for a medical checkup for the longest period of time. The users will be able to register into the system by providing their information. They dont need to go to the doctor to varify what they were having for the few couple of days.they can say by their own that what is their(blood pressure,suger level,diabeties) etc through out this week. They will also be able to access the system through the web or via a mobile application.
		</blockquote>
	</p>

	<h4 >&emsp;1.3&emsp;Intended Audience and Document Overview</h4>
	<p>
		<blockquote>
			This project is for helping people to check their medical report in a daily basis so that they will also have an update what they are eating and what changes they should make to have a healthy life. So the intended audience of the project is the general population in this world. On the other hand, this document which describes the project is intended to be read by the developers who will program it, the faculty who will supervise it and for anyone who would like to understand how this project functions and what it intends to achieve. This document is designed in a way that the developers should read the sections 3,4  for learning about the features that needs to be implemented and also for learning about the requirements that the system must provide. For the non-technical persons, it would be more appropriate to refer to the sections 1 and 2. 
		</blockquote>
	</p>
	
	<h4 align="left">&emsp;1.4&emsp;Definitions, Acronyms and Abbreviations</h4>
	<p>
		<blockquote>
			Here are some of the acronyms that are used throughout this document: 
            SRS – Software Requirement Specification.
            API - Application Protocol Interface. This is the part of a program that lets other programs or services interact with the data in the former and viceversa.
		</blockquote>
	</p>
	
	<h4 align="left">&emsp;1.5&emsp;Document Convention</h4>
	<p>
		<blockquote>
			This document follows the standard typography of using the font "Calibri" with a font size of 12 to make it easy for the readers to read it.for the Sub Section Titles we use  Font "Calibri", Face: Bold and font  Size is  14 .
		</blockquote>
	</p>
    <h4 align="left">&emsp;1.6&emsp;References and Acknowledgments</h4>
	<p>
		<blockquote>
			We will use google and some other websites for doing this project.
		</blockquote>
	</p>
    <p style="page-break-after:always"></p>	
    <h2 align="left">2.	Overall Description</h2>
	<h4 align="left">&emsp;2.1&emsp;Product Overview</h4>
	<p>
		<blockquote>
			The product is a new, self-contained product. No previous versions were launched. The app is basically a online storage device specifically made to store reports of medical patients by scanning the hard copy of their reports. Each user creates their own account and uploads their personal information via speech recognition. They can then scan and keep a digital copy or their medical report online. When needed they can search and look at their medical records for a particular date which is also done using speech recognition. Or, they can see specific aspects of their medical history (e.g. blood pressure, blood sugar levels, etc.) in graphical form.
		</blockquote>
	</p>

	<h4 align="left">&emsp;2.2&emsp;Product Functionality</h4>
	<p>
		<ul>
			<li>Users can create an account.</li>
			<li>Upload their personal information via speech recognition.</li>
			<li>Scan their medical reports, keeping a digital copy of each.</li>
			<li>Search and look at their medical records for a particular date using speech recognition.</li>
			<li>See specific aspects of their medical history (e.g. blood pressure, blood sugar levels, etc.) in graphical form.</li>
		</ul>
	</p>

	<h4 >&emsp;2.3&emsp;Design and Implementation Constraints</h4>
	<p>
		<blockquote>
			Users need to ensure that they speak clearly and loudly while relaying their personal information onto the app and while conducting specific searches. For developing android versions, developers must know java and for the web version, a php framework. For communication protocol, developers must be familiar with git and Bitbucket.
		</blockquote>
	</p>
	
	<h4 align="left">&emsp;2.4&emsp;Assumptions and Dependencies</h4><p>
		<ul>
			<li>Signing in by google in this software will be reused from one of the Google's API. And it should work with similar efficiency.</li>
			<li>Google vision API is used to search for specific text in scanned images and VanCharts/ Google Charts can be used to show the searches in a graphical form.</li>
			<li>A third party text-to-speech API will be used for the speech recognition parts of the product.</li>
		</ul>
	</p>
<p style="page-break-after:always"></p>
<h2 align="left">3. External Interface Requirements</h2>
	<h4 align="left">&emsp;3.1.1&emsp;User Interfaces</h4>
	<p>
		<blockquote>
			The user interface for the web version: 
             <ol><li>There will be a navigation bar which will have the buttons named “Sign up”, “Feautures”, “Resources”, “About”.</li>
              <li>The home page will have the login fields and a button to submit.</li>
              <li>If logged in the user will be taken to a page describing the way, the system works and how to use it.</li>
              <li>The camera will scan the  image of report.</li>
              <li>After the image analysis is, the user would be shown a graphical description of the report.</li>
          </ol>
             The user interface for the mobile Application version:<br><blockquote>The UI for the app will roughly work the same way as the web version. The difference being that the navigation bar’s part will be located in the navigation drawer of the app. The other features will work the same way. </blockquote>
		</blockquote>
	</p>

	<h4 align="left">&emsp;3.1.2&emsp;Hardware Interfaces</h4>
	<p><blockquote>Neither the mobile application nor the web version has any special hardware requirements. So, the system will not have any hardware interface.  The camera required for AR experience is assumed to be provided by the mobile phones or other devices where the app will be installed.</blockquote>
	</p>

	<h4 >&emsp;3.1.3&emsp;Software Interfaces</h4>
	<p>
		<blockquote>
			Both the mobile application and the website will communicate with the database via internet. The operating system will provide such interfaces. Both the platforms will consist of operations regarding the reading and writing of data.
		</blockquote>
	</p>

	<h4 >&emsp;3.2&emsp;Functional Requirements</h4>
	<p>
		<blockquote>
			
		</blockquote>
	</p>

	<p style="page-break-after:always"></p>	<br><br><br><br>
    <h4 >&emsp;3.3&emsp;Case Model</h4>
	<p>
		<blockquote>
			
		</blockquote>
	</p>
<div style="margin-top: 200px"></div>
<p style="page-break-after:always"></p>	
    <h2 align="left">4.	Other Nonfunctional Requirements</h2>
	<h4 align="left">&emsp;4.1&emsp;Performance Requirements</h4>
	<p>
		<blockquote>
			The app should be a light app and it should not a huge chunk of the user's storagem since everything is stored online. So users with limited storage space will also be able to use the app.
		</blockquote>
	</p>

	
	<h4 >&emsp;4.2&emsp;Safety & Security Requirements</h4>
	<p>
		<blockquote>
			No data is lost in case the user decides to change devices or if the user's device is extensively damaged since all data will be stored in cloud storages. So, like any other system, security is a priority in this system as well. This will allow safe storage and easy accessibility from anywhere.
		</blockquote>
		<blockquote>
			The System shall not disclose any personal information about the users. The Application shall not grant access to an unauthorized user and the Application shall not communicate with any other devices or servers while in use by the user.
		</blockquote>
	</p>
	
	<h4 align="left">&emsp;4.3&emsp;Software Quality Attributes</h4><p>
		<blockquote>The requirements of this software quality attributes is to Correctness, availability to store data, Usability and also fexibility of the software.</blockquote>
	</p>


<p style="page-break-after:always"></p>	<br><br><br><br>
    <h2 align="left">5.	Other Requirements</h2>
	<p>
		<blockquote>
		This softwares needs to be able to work on different platforms. It should also be able to work with different frameworks.So that other software develpers can build on it or edit it easily when the software requires an update.It will also require multiple third party APIs to work in its intended fashion. 
		</blockquote>
	</p>

<p style="page-break-after:always"></p>	<br><br><br><br>
    <h2 align="left">APPENDIX A – DATA DICTIONARY </h2>
	<p>
		
	</p>


<p style="page-break-after:always"></p>	<br><br><br><br>
    <h2 align="left">APPENDIX B – GROUP LOG </h2>
	<p>
		
	</p>











<div style="margin-top: 200px"></div>
<center><button id="printbtn" onClick="window.print();"><h1>Print</h1></button></center>

</body>
</html>